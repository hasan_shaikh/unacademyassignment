package com.example.unacademyassignment;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.example.unacademyassignment.databinding.ActivityMainBinding;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {
    public static final String IMAGE_1 = "https://cdn.vox-cdn.com/thumbor/rpoemOnWYnH8z_JOlK3jWdi34dQ=/0x0:2040x1360/1200x800/filters:focal(857x517:1183x843)/cdn.vox-cdn.com/uploads/chorus_image/image/66557155/wjoel_180413_1777_android_001.0.jpg";
    public static final String IMAGE_2 = "https://mylargebox.com/wp-content/uploads/2019/11/Android.jpg";
    String tag = MainActivity.class.getSimpleName();
    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_main);
        binding.button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fetchImage1AndSet();
            }
        });

        binding.button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fetchImage2AndSet();
            }
        });

    }

    private void fetchImage1AndSet() {
        ServiceWorker serviceWorker = new ServiceWorker("ServiceWorker1");
        serviceWorker.addTask(new ServiceWorker.Task<Bitmap>() {
            @Override
            public Bitmap onExecuteTask() throws IOException {
                Log.v(tag,"onExecuteTask1");
                Request request = new Request.Builder().url(IMAGE_1).build();
                Response response = new OkHttpClient().newCall(request).execute();
                final Bitmap bitmap = BitmapFactory.decodeStream(response.body().byteStream());
                return bitmap;
            }

            @Override
            public void onTaskComplete(Bitmap result) {
                Log.v(tag,"onTaskComplete1");
                binding.imageView1.setImageBitmap(result);
            }
        });
    }

    private void fetchImage2AndSet() {
        ServiceWorker serviceWorker = new ServiceWorker("ServiceWorker2");
        serviceWorker.addTask(new ServiceWorker.Task<Bitmap>() {
            @Override
            public Bitmap onExecuteTask() throws IOException {
                Log.v(tag,"onExecuteTask2");
                Request request = new Request.Builder().url(IMAGE_2).build();
                Response response = new OkHttpClient().newCall(request).execute();
                final Bitmap bitmap = BitmapFactory.decodeStream(response.body().byteStream());
                return bitmap;
            }

            @Override
            public void onTaskComplete(Bitmap result) {
                Log.v(tag,"onTaskComplete2");
                binding.imageView2.setImageBitmap(result);
            }
        });
    }
}
