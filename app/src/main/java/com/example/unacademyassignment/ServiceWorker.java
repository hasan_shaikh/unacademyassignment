package com.example.unacademyassignment;

import android.os.Handler;
import android.os.Looper;

import java.io.IOException;

public class ServiceWorker {
    private ThreadPool threadPool;

    private String tag;

    public ServiceWorker(String tag) {
        this.tag = tag;
        threadPool = new ThreadPool();
    }

    public String getTag() {
        return tag;
    }

    void addTask(final Task task) {
        threadPool.getmThreadPoolExecutor().execute(new Runnable() {
            @Override
            public void run() {
                Object res = null;
                try {
                    res = task.onExecuteTask();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Handler h = new Handler(Looper.getMainLooper());
                final Object finalRes = res;
                h.post(new Runnable() {
                    @Override
                    public void run() {
                        task.onTaskComplete(finalRes);
                    }
                });
            }
        });

    }

    //Task interface
    public interface Task<T> {
        T onExecuteTask() throws IOException;
        void onTaskComplete(T result);
    }
}
